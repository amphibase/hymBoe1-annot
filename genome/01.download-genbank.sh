#!/bin/bash

## RefSeq
curl --output-dir ./ -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/019/447/015/GCA_019447015.1_UCB_Hboe_1.0/GCA_019447015.1_UCB_Hboe_1.0_assembly_report.txt
curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/019/447/015/GCA_019447015.1_UCB_Hboe_1.0/GCA_019447015.1_UCB_Hboe_1.0_genomic.fna.gz

curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/019/447/015/GCA_019447015.1_UCB_Hboe_1.0/GCA_019447015.1_UCB_Hboe_1.0_genomic.gff.gz
curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/019/447/015/GCA_019447015.1_UCB_Hboe_1.0/GCA_019447015.1_UCB_Hboe_1.0_protein.faa.gz
curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/019/447/015/GCA_019447015.1_UCB_Hboe_1.0/GCA_019447015.1_UCB_Hboe_1.0_protein.fna.gz
